extends Node2D

export var trezora = false
#export var cel_sceno
onready var animacioj = $pordo_animacioj

func _ready():
	$partikloj.set_emitting(false)
	if trezora:
		var novpordo = load('res://grafikajxoj/pordoj/la_pordo_orangxa.png')
		$la_pordo.set_texture(novpordo)
		$partikloj.set_modulate(Color(0.8,0.5,0,1))
	else:
		pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Area2D_body_entered(body):
	animacioj.play("malfermi")
	$partikloj.set_emitting(true)


func _on_Area2D_body_exited(body):
	animacioj.play_backwards("malfermi")
	$partikloj.set_emitting(false)