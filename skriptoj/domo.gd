extends StaticBody2D

export(float) var sxalttempo = 1
export(bool) var sxaltita = false
export(bool) var sxaltigebla = true

func _ready():
	

	if sxaltita == true:
		$malfermo.play("malfermo", -1, 10, false)
		funkciigxi(true)
	else:
		funkciigxi(false)

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func sxaltigxi(ningxao):
	#se la domo ankorau ne ests saltita ni saltas la konstruadon se estis
	if !sxaltita:
		konstruigxi(ningxao)
	else:
		pass

func konstruigxi(ningxao):
	$malfermo.play("malfermo")
	sxaltigebla = false
	yield(get_tree().create_timer(5),"timeout")
	sxaltita = true
	sxaltigebla = true
	funkciigxi(true)
	var korpoj = $loko2d.get_overlapping_bodies()
	for korpo in korpoj:
		if korpo.name == "ningxao":
			ningxao.sxaltebligi(self)

func _on_loko2d_body_entered(korpo):
	if korpo.name == "ningxao" and sxaltigebla:
		korpo.sxaltebligi(self)

func _on_loko2d_body_exited(korpo):
	if korpo.name == "ningxao" and sxaltigebla:
		korpo.malsxaltebligi(self)

func funkciigxi(vere):
	var fajroj = $fajro.get_children()
	if vere:
		for fajro in fajroj:
			fajro.set_emitting(true)
			$fajr_sonoj/fajrsono.play(0.1)
	else:
		for fajro in fajroj:
			fajro.set_emitting(false)
			$fajr_sonoj/fajrsono.stop()

func _on_AudioStreamPlayer2D_finished():
	if sxaltita:
		$fajr_sonoj/fajrsono.play(0.1)
